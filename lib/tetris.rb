module Tetris
end

require 'tetris/sprite'
require 'tetris/scene'
require 'tetris/game'
require 'tetris/settings'
require 'tetris/window'
require 'tetris/sizes'