class Tetris::Game::Figure < Tetris::Game::Base
  attr_reader :figure, :row, :column

  class << self
    attr_accessor :next

    def register!
      klasses << self
    end

    def random(field)
      current = @next || calculate_next(field)
      @next = calculate_next(field)
      current
    end

    def calculate_next(field)
      klasses.sample.new(field).tap do |figure|
        (1..3).to_a.sample.times { figure.turn! }
      end
    end

    private

    def klasses
      @@klasses ||= []
    end
  end

  def initialize(field)
    @matrix = field.matrix
    @figure = self.class.figure
    @row = -1
    @column = (@matrix.first.length - @figure.first.length) / 2
  end

  def left!
    @column -= 1
  end

  def right!
    @column += 1
  end

  def down!
    @row += 1
  end

  def turn!
    res = Array.new(@figure.first.length) { [] }
    old_figure = @figure
    @figure = @figure.each.with_object(res) do |row, res|
      row.each.with_index do |el, i|
        res[i].unshift el
      end
    end
    @figure = old_figure if overlaps? 
  end

  def landed?
    landed_on_bottom? || landed_on_element?
  end

  def full?
    @row >= @figure.length - 1
  end

  def blocks_left?
    blocks_on_field_left? || blocks_left_on_element?
  end

  def blocks_right?
    blocks_on_field_right? || blocks_right_on_element?
  end

  def figure_elements(&block)
    matrix_elements(@figure, &block)
  end

  private

  def overlaps?
    return true if @column < 0 || @column + @figure.first.size > @matrix.first.size
    return true if @row >= @matrix.size
    all_elements do |h, w, f_h, f_w|
      return true if same_column?(f_w, w) && same_row?(f_h, h) 
    end
    false
  end

  def blocks_on_field_left?
    @column <= 0
  end

  def blocks_on_field_right?
    @column + @figure.first.size >= @matrix.first.size
  end
  
  def landed_on_bottom?
    @row >= @matrix.length - 1
  end

  def blocks_left_on_element?
    check_all_elements do |height, width, figure_height, figure_width|
      same_column?(figure_width, width, -1) && same_row?(figure_height, height)
    end
  end

  def blocks_right_on_element?
    check_all_elements do |height, width, figure_height, figure_width|
      same_column?(figure_width, width, 1) && same_row?(figure_height, height)
    end
  end

  def landed_on_element?
    check_all_elements do |height, width, figure_height, figure_width|
      same_column?(figure_width, width) && same_row?(figure_height, height, 1)
    end
  end

  def all_elements
    elements { |h, w| figure_elements { |f_h, f_w| yield h, w, f_h, f_w } }
  end

  def check_all_elements
    all_elements do |h, w, f_h, f_w|
      return true if yield h, w, f_h, f_w
    end
    false
  end

  def same_column?(figure_width, width, offset = 0)
    figure_width + @column + offset == width
  end

  def same_row?(figure_height, height, offset = 0)
    @row - @figure.length + figure_height + 1 + offset == height
  end
end

require 'tetris/game/figure/square'
require 'tetris/game/figure/gose'
require 'tetris/game/figure/reversed_gose'
require 'tetris/game/figure/t'
require 'tetris/game/figure/line'
require 'tetris/game/figure/z'
require 'tetris/game/figure/reversed_z'