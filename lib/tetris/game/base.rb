class Tetris::Game::Base
  attr_reader :matrix

  def elements(&block)
    matrix_elements(@matrix, &block)
  end

  private

  def matrix_elements(matrix)
    Marshal.load(Marshal.dump matrix).each.with_index do |row, h|
      row.each.with_index { |el, w| yield h, w if el }
    end
  end
end