class Tetris::Game::Field < Tetris::Game::Base
  def initialize(game)
    @game = game
    @matrix = load_matrix
  end

  def add_figure
    @figure = Tetris::Game::Figure.random(self)
  end

  def merge!
    @overflowed = !@figure.full?
    figure_elements do |h, w|
      @matrix[h][w] = 1 if h >= 0 && w >= 0
    end
    destroy_layer
    @figure = nil
  end

  def overflowed?
    !!@overflowed
  end

  def left!
    @figure.left! unless @figure.blocks_left?
  end

  def right!
    @figure.right! unless @figure.blocks_right?
  end

  def down!
    @figure.down! unless landed?
  end

  def turn!
    @figure.turn!
  end

  def landed?
    @figure.landed?
  end

  def field_elements(&block)
    elements(&block)
    figure_elements(&block)
  end

  private

  def destroy_layer
    @matrix.map.with_index { |row, i| i if row.all? }.compact.each do |i|
      @matrix.delete_at i 
      @matrix.unshift [nil] * Tetris::Settings.width
      @game.add_score Tetris::Settings.per_layer_score
    end
  end

  def figure_elements
    matrix_elements @figure.figure do |h, w|
      yield @figure.row - @figure.figure.length + h + 1, w + @figure.column
    end
  end

  def load_matrix
    Array.new Tetris::Settings.height do
      [nil] * Tetris::Settings.width
    end
  end
end