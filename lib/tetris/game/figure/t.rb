class Tetris::Game::Figure::T < Tetris::Game::Figure
  def self.figure
    [
      [1, 1, 1],
      [nil, 1, nil]
    ]
  end

  register!
end