class Tetris::Game::Figure::Square < Tetris::Game::Figure
  def self.figure
    [
      [1, 1],
      [1, 1]
    ]
  end

  register!
end