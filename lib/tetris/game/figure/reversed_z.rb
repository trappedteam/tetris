class Tetris::Game::Figure::ReversedZ < Tetris::Game::Figure
  def self.figure
    [
      [nil, 1, 1],
      [1, 1, nil]
    ]
  end

  register!
end