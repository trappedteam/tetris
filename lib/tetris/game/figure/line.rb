class Tetris::Game::Figure::Line < Tetris::Game::Figure
  def self.figure
    [
      [1, 1, 1, 1]
    ]
  end

  register!
end