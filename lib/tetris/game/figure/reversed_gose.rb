class Tetris::Game::Figure::ReversedGose < Tetris::Game::Figure
  def self.figure
    [
      [1, 1],
      [nil, 1],
      [nil, 1]
    ]
  end

  register!
end