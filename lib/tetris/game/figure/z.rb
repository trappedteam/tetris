class Tetris::Game::Figure::Z < Tetris::Game::Figure
  def self.figure
    [
      [1, 1, nil],
      [nil, 1, 1]
    ]
  end

  register!
end