class Tetris::Sprite < Sprite_Base
  def initialize(e_height, e_width, window, padding = 0, multiplier = 1)
    super()
    @e_height, @e_width = e_height, e_width
    @window, @padding, @multiplier = window, padding, multiplier
    self.visible = false
    create_bitmap
    set_position
    set_color
  end

  def update
    super
    self.visible = @e_height >= 0
  end

  private

  def set_color
    self.bitmap.fill_rect 1, 1, width - 1, height - 1, color
  end

  def color
    Color.new(*Tetris::Settings.color)
  end

  def set_position
    self.x = @e_width * width + 2 + @window.x + @padding
    self.y = @e_height * height + @window.y + @padding
  end

  def create_bitmap
    self.bitmap = Bitmap.new width, height
  end

  def width
    Tetris::Sizes.cell * @multiplier
  end

  def height
    Tetris::Sizes.cell * @multiplier
  end
end