class Tetris::Window::Text < Window_Base
  LINE_HEIGHT = 36

  def initialize(game, *args)
    @game = game
    super(*args)
  end

  def update
    super
    if @text != calculate_text
      @text = calculate_text
      alert
    end
  end

  private

  def alert
    contents.clear
    @text.lines.each.with_index do |line, index|
      contents.draw_text(*sizes(index), line.strip)
    end
  end

  def sizes(index)
    [0, LINE_HEIGHT * index, contents.rect.width, LINE_HEIGHT * (index + 1)]
  end
end

require 'tetris/window/text/level'
require 'tetris/window/text/score'