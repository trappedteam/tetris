class Tetris::Window::Field < Window_Base
  def initialize(game, *args)
    @game = game
    @sprites = {}
    super(*args)
  end

  def update
    super
    update_sprites
  end

  def dispose
    super
    dispose_sprites
  end

  def update_sprites
    dispose_sprites_except create_sprites
    @sprites.values.each(&:update)
  end

  def dispose_sprites_except(keys)
    (@sprites.keys - keys).each do |key| 
      @sprites[key].dispose
      @sprites.delete key
    end
  end

  def create_sprites
    keys = []
    @game.field.field_elements do |height, width|
      keys << [height, width]
      @sprites[[height, width]] ||= Tetris::Sprite.new(height, width, self)
    end
    keys
  end

  def dispose_sprites
    @sprites.values.each(&:dispose)
  end
end