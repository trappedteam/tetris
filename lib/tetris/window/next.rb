class Tetris::Window::Next < Window_Base
  def initialize(game, *args)
    @game = game
    @sprites = []
    @next = nil
    super(*args)
  end

  def update
    super
    if Tetris::Game::Figure.next != @next 
      dispose_sprites
      @next = Tetris::Game::Figure.next
      create_sprites
    end
    update_sprites
  end

  def dispose
    super
    dispose_sprites
  end

  def update_sprites
    @sprites.each(&:update)
  end

  def create_sprites
    @sprites = []
    @next.figure_elements do |h, w|
      @sprites << Tetris::Sprite.new(h, w, self, 50, 1.6)
    end
  end

  def dispose_sprites
    @sprites.each(&:dispose)
  end
end