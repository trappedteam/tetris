#encoding=utf-8

class Tetris::Window::Text::Score < Tetris::Window::Text
  def calculate_text
    "Cчет: #{@game.score}"
  end
end