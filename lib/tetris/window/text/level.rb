#encoding=utf-8

class Tetris::Window::Text::Level < Tetris::Window::Text
  def calculate_text
    "Уровень: #{@game.level}"
  end
end