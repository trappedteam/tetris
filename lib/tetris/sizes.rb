module Tetris::Sizes
  def cell
    if Tetris::Settings.height > Tetris::Settings.width
      (Graphics.height / Tetris::Settings.height.to_f).to_i
    else
      ((Graphics.width * 0.66) / Tetris::Settings.width).to_i
    end 
  end

  def window_width
    cell * Tetris::Settings.width + 4
  end

  def window_height
    cell * Tetris::Settings.height + 4
  end

  def padding
    Graphics.height - window_height
  end

  def info_width
    Graphics.width - window_width 
  end

  def next_height
    Graphics.height / 2
  end

  def score_height
    Graphics.height / 4
  end

  def level_height
    Graphics.height / 4
  end

  extend self
end