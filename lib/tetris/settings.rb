module Tetris::Settings
  DEFAULTS = {
    speed: 40,
    width: 12,
    height: 18,
    level_score: 2000,
    per_layer_score: 400,
    color: [0, 255, 0]
  }

  DEFAULTS.keys.each do |key|
    instance_eval { attr_writer key }

    define_method key do 
      instance_variable_get("@#{key}") || DEFAULTS[key]
    end
  end

  extend self
end