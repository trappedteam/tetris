class Tetris::Scene < Scene_Base
  def start
    super
    @game = Tetris::Game.new self
    @game.start
    create_windows
  end

  def terminate
    @game = nil
    Tetris::Game::Figure.next = nil
    super
  end

  def update
    check_keys
    @game.update
    super
  end

  private

  def create_windows
    create_field_window
    create_next_window
    create_score_window
    create_level_window
  end

  def create_next_window
    @next_window = Tetris::Window::Next.new(
      @game, Tetris::Sizes.window_width, 0,
      Tetris::Sizes.info_width, Tetris::Sizes.next_height
    )
  end

  def create_score_window
    @score_window = Tetris::Window::Text::Score.new(
      @game, Tetris::Sizes.window_width, Tetris::Sizes.next_height,
      Tetris::Sizes.info_width, Tetris::Sizes.score_height
    )
  end

  def create_level_window
    @level_window = Tetris::Window::Text::Level.new(
      @game, Tetris::Sizes.window_width, Tetris::Sizes.next_height + Tetris::Sizes.score_height,
      Tetris::Sizes.info_width, Tetris::Sizes.level_height
    )
  end

  def create_field_window
    @field_window = Tetris::Window::Field.new(
      @game, 0, Tetris::Sizes.padding,
      Tetris::Sizes.window_width, Tetris::Sizes.window_height
    )
  end


  def check_keys
    @game.left! if Input.trigger? Input::LEFT 
    @game.right! if Input.trigger? Input::RIGHT
    @game.down! if Input.press? Input::DOWN
    @game.turn! if Input.trigger? Input::UP
  end
end