class Tetris::Game
  attr_reader :field, :score, :level

  def initialize(scene)
    @scene = scene
    @field = Tetris::Game::Field.new self
    @level = 1
    @score = 0
    @ticked = -1
  end

  def start
    @field.add_figure
  end

  def update
    return if @game_overed
    @ticked += 1
    if @ticked % speed == 0
      down!
      try_game_over
    end
  end

  def terminate
    @scene.terminate
  end

  def left!
    @field.left!
    try_merge
  end

  def right!
    @field.right!
    try_merge
  end

  def down!
    try_merge
    @field.down!
  end

  def turn!
    try_merge
    @field.turn!
  end

  def add_score(points)
    @score += points
    @level = @score / Tetris::Settings.level_score + 1
  end

  private

  def speed
    (Tetris::Settings.speed / @level.to_f).to_i
  end

  def game_over
    @game_overed = true
    SceneManager.call Scene_Map
  end

  def try_merge
    if @field.landed?
      @field.merge! 
      @field.add_figure
    end
  end

  def try_game_over
    game_over if @field.overflowed?
  end
end

require 'tetris/game/base'
require 'tetris/game/figure'
require 'tetris/game/field'